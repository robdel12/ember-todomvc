import { module, test } from "qunit";
import { visit, currentURL } from "@ember/test-helpers";
import { setupApplicationTest } from "ember-qunit";
import { percySnapshot } from "ember-percy";
import TodoMVC from "todomvc/tests/interactors/app";
import { when } from "@bigtest/convergence";

module("Acceptance | app test", function(hooks) {
  let TodoApp = new TodoMVC();
  setupApplicationTest(hooks);
  hooks.beforeEach(function() {
    window.localStorage.clear();
  });

  test("add todos", async function(assert) {
    await visit("/");

    await percySnapshot("Empty todo list");

    // create 1st todo
    await TodoApp.newTodo("My First Todo").submitTodo();

    // make sure the 1st label contains the 1st todo text
    await when(() => assert.equal(TodoApp.todoList(0).todoText, "My First Todo"));

    // create 2nd todo
    await TodoApp.newTodo("My Second Todo").submitTodo();

    // make sure the 2nd label contains the 2nd todo text
    await when(() => assert.equal(TodoApp.todoList(1).todoText, "My Second Todo"));

    await assert.equal(currentURL(), "/");

    await percySnapshot("Todo list with 2 todos");
  });

  test("completing a todo", async function(assert) {
    await visit("/");
    await TodoApp.createTwoTodos();

    // assert we have two todos
    await when(() => assert.equal(TodoApp.todoList().length, 2));
    await percySnapshot("Todo list with 2 todos");

    // Complete the first todo
    await TodoApp.todoList(0).toggle();
    await when(() => assert.equal(TodoApp.todoList(0).isCompleted, true));
    await percySnapshot("Todo list with 2 todos, one completed");
  });

  test("editing a todo by pressing enter", async function(assert) {
    await visit("/");
    await TodoApp.createTwoTodos();
    // enter into the editing state
    await TodoApp.todoList(0).doubleClick();

    // Make sure the editing state is correct
    await when(() => assert.equal(TodoApp.todoList(0).labelIsVisible, false));
    await when(() => assert.equal(TodoApp.todoList(0).toggleIsVisible, false));

    // Take a snapshot
    await percySnapshot("Editing a todo list");

    // edit the todo
    await TodoApp.todoList(0)
      .only()
      .fillInput("Newly edited")
      .pressEnter();

    await percySnapshot("Submit edited todo");
    await when(() => assert.equal(TodoApp.todoList(0).todoText, "Newly edited"));
  });

  test("editing a todo by blurring", async function(assert) {
    await visit("/");
    await TodoApp.createTwoTodos();
    // enter into the editing state
    await TodoApp.todoList(0).doubleClick();

    // Make sure the editing state is correct
    await when(() => assert.equal(TodoApp.todoList(0).labelIsVisible, false));
    await when(() => assert.equal(TodoApp.todoList(0).toggleIsVisible, false));

    // edit the todo
    await TodoApp.todoList(0)
      .only()
      .fillInput("Newly edited")
      .blurInput();

    await when(() => assert.equal(TodoApp.todoList(0).todoText, "Newly edited"));
  });

  test("displays the current number of todo items", async function(assert) {
    await visit("/");
    await TodoApp.createTwoTodos();
    await when(() => assert.equal(TodoApp.todoCountText, "2 items left"));
    await when(() => assert.equal(TodoApp.todoList().length, 2));
    await percySnapshot("Footer display count before completing");

    // Complete the first one
    await TodoApp.todoList(0).toggle();

    await when(() => assert.equal(TodoApp.todoCountText, "1 item left"));
    await percySnapshot("Footer display count after completing");

    // because we're on the all filter it should still include the completed todo
    await when(() => assert.equal(TodoApp.todoList().length, 2));
    await when(() => assert.equal(TodoApp.allFilterHasSelectedClass, true));
  });

  test("filtering to completed todos", async function(assert) {
    await visit("/");
    await TodoApp.createTwoTodos();
    await TodoApp.todoList(0)
      .toggle()
      .clickCompleteFilter();

    await when(() => assert.equal(TodoApp.completedFilterHasSelectedClass, true));
    await when(() => assert.equal(TodoApp.todoList().length, 1));
    await percySnapshot("Filtering by completed todos");
  });
});
